import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AlbumsService {

  private albumsEndpoint = 'https://freemusicarchive.org/api/get/albums.json?api_key=G89WVOFIAUX2NHPA';
  private albumDetailsEndpoint = 'https://freemusicarchive.org/api/get/album.json?album_id='

  constructor(private http: Http) { }

  getOneAlbum(albumId: number) {
    let url = `${this.albumDetailsEndpoint}${albumId}`;
    return this.http.get(url)
      .map((response: Response) => response.json())
      .catch(this.handleError)
  }

  getAlbums() {
    return this.http.get(this.albumsEndpoint)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    return response.json().dataset;
  }

  private handleError(error: Response | any) {
    let errorMsg: string;
    return Observable.throw(errorMsg);
  } 
}
