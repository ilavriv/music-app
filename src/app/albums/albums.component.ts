import { Component, OnInit } from '@angular/core';
import { AlbumsService } from '../albums.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  albums: any[] = [];
  loading: boolean = false;

  constructor(private albumsService: AlbumsService) { }

  ngOnInit() {
    this.loading = true;
    this.albumsService.getAlbums().subscribe(
      albums => Object.assign(this, {
        loading: false,
        albums
      }),
      error => console.log(error)
    );
  }
}
