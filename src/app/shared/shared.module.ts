import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Http, Request } from '@angular/http';
import { environment } from '../../environments/environment'

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SharedModule {
  static dataSet(datasetName: string) {
    return `${environment.apiUrl}/${datasetName}?api_key=${environment.apiKey}`;
  }
}
