import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AlbumsService } from "../albums.service";

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.css']
})
export class AlbumDetailsComponent implements OnInit {

  album: any = null;
  loading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private albumsService: AlbumsService
  ) { }

  ngOnInit() {
    this.loading = true;
    let id = +this.route.snapshot.params['id'];
    this.albumsService.getOneAlbum(id)
      .subscribe(
        album => {
          this.album = album;
          this.loading = false;
        },
        error => {
          console.log("error", error);
        }
      );
  }

}
