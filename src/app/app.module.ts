import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { AlbumsComponent } from './albums/albums.component';
import { ArtistsComponent } from './artists/artists.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AlbumDetailsComponent } from './album-details/album-details.component';

import { AlbumsService } from './albums.service';
import { ArtistsService } from './artists.service';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    AlbumsComponent,
    ArtistsComponent,
    ContactsComponent,
    AlbumDetailsComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule.forRoot([{
      path: '',
      component: IndexComponent
    }, {
      path: 'albums',
      component: AlbumsComponent
    }, {
      path: 'albums/:id',
      component: AlbumDetailsComponent
    }, {
      path: 'artists',
      component: ArtistsComponent
    }, {
      path: 'contacts',
      component: ContactsComponent
    }])
  ],
  providers: [
    AlbumsService,
    ArtistsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
