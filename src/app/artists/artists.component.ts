import { Component, OnInit } from '@angular/core';
import { ArtistsService } from '../artists.service';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.css']
})
export class ArtistsComponent implements OnInit {
  artists: any[] = [];
  loading: boolean = false;

  constructor(private artistsService: ArtistsService) { }

  ngOnInit() {
    this.loading = true;
    this.artistsService.getArtists()
      .subscribe(
        artists => Object.assign(this, {
            loading: false,
            artists
        }),
        error => console.log("error", error)
      )
  }

}
