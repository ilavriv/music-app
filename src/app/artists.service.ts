import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ArtistsService {

  private artistsEndpoint = 'https://freemusicarchive.org/api/get/artists.json?api_key=G89WVOFIAUX2NHPA';

  constructor(private http: Http) { }

  getArtists() {
    return this.http.get(this.artistsEndpoint)
      .map(this.handleResponse)
      .catch(this.handleError);
  }

  private handleResponse(response: Response) {
    return response.json().dataset;
  }

  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
}
